# Frontend Test Task

## The task

Create a useful app using [The Bored API](https://www.boredapi.com/).

1. Fork this repo
2. Add CONTRIBUTORS.md with your name in it
3. Code
4. Submit a new merge request to this repo

## Requirements

### UI

- Make it pretty

### API usage

- Make use of query parameters

### Technologies

- Use React with Hooks
- Use styled-components
